<?php
namespace AppBundle\Service;

use AppBundle\Client\ApiClient;
use AppBundle\Client\FFMpegClient;
use GuzzleHttp\Client;

class VideoService
{
    /**
     * @var string
     */
    protected $videoDirectory;

    /**
     * @var string
     */
    protected $screenshotDirectory;

    /**
     * @var FFMpegClient
     */
    protected $ffmpegClient;

    /**
     * @var ApiClient
     */
    protected $apiClient;

    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * VideoService constructor.
     *
     * @param FFMpegClient $ffmpegClient
     * @param ApiClient $apiClient
     * @param Client $httpClient
     */
    public function __construct($ffmpegClient, $apiClient, $httpClient)
    {
        $this->ffmpegClient = $ffmpegClient;
        $this->apiClient = $apiClient;
        $this->httpClient = $httpClient;
    }

    /**
     * Set the video storage directory 
     * 
     * @param $videoDirectory
     */
    public function setVideoDirectory($videoDirectory)
    {
        $this->videoDirectory = $videoDirectory;
    }

    /**
     * Set the screenshot storage directory
     * 
     * @param $screenshotDirectory
     */
    public function setScreenshotDirectory($screenshotDirectory)
    {
        $this->screenshotDirectory = $screenshotDirectory;
    }

    /**
     * Physically download the video to the filesystem
     * 
     * @param $url
     * @param $target
     */
    protected function downloadVideo($url, $target)
    {
        $this->httpClient->get($url, [
            'sink' => $target,
        ]);
    }

    /**
     * Take a screenshot from a video by video ID and offset (in seconds)
     * 
     * @param string $videoId
     * @param int $offset
     * @return string
     */
    public function generateScreenshot($videoId, $offset)
    {
        $hash = md5(time());
        $downloadUrl = $this->apiClient->getVideoCdnUrl($videoId);

        $videoFile = sprintf('%s/%s.tmp', $this->videoDirectory, $hash);
        $this->downloadVideo($downloadUrl, $videoFile);
        
        $ssFile = sprintf('%s/%s.jpg', $this->screenshotDirectory, $hash);
        $this->ffmpegClient->makeScreenshot($videoFile, $ssFile, $offset);
        
        unlink($videoFile);
        
        return sprintf('%s.jpg', $hash);
    }
}