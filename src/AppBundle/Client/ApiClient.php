<?php
namespace AppBundle\Client;

use GuzzleHttp\Client;

class ApiClient
{
    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * @var
     */
    protected $accessToken;

    /**
     * @var array
     */
    protected $videoManager;

    /**
     * ApiClient constructor.
     *
     * @param string $username
     * @param string $password
     */
    public function __construct($apiBaseUrl, $username, $password)
    {
        $this->httpClient = new Client([
            'base_uri' => $apiBaseUrl,
        ]);

        $this->authenticate($username, $password);
    }

    /**
     * Send authentication headers
     *
     * @return array
     */
    protected function getAuthenticationHeaders()
    {
        return ['Authorization' => 'Bearer ' . $this->accessToken];
    }

    /**
     * Authenticate and obtain an access token
     *
     * @param string $username
     * @param string $password
     */
    protected function authenticate($username, $password)
    {
        $response = $this->httpClient->post('auth/login', [
            'json' => [
                'username' => $username,
                'password' => $password,
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $data = \json_decode($response->getBody(), true);
            $this->accessToken = $data['accessToken'];
            $this->videoManager = $data['videoManagerList'][0];
        }
    }

    /**
     * Retrieve the first CDN url for a video by ID
     * @param string $videoId
     * @return string
     */
    public function getVideoCdnUrl($videoId)
    {
        $url = sprintf('%s/videos/%s/download-urls', $this->videoManager['id'], $videoId);

        $response = \json_decode($this->httpClient->get($url, [
            'headers' => $this->getAuthenticationHeaders()
        ])->getBody(), true);

        return $response[0]['url'];
    }
}
