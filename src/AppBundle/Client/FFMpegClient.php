<?php
namespace AppBundle\Client;

class FFMpegClient
{
    /**
     * Turn an integer into hh:mm:ss notation 
     * 
     * @param int $offset
     * @return string
     */
    protected function convertOffset($offset)
    {
        $t = round($offset);
        return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
    }

    /**
     * Generate a screenshot using FFMpeg
     * 
     * @param string $filename
     * @param string $output
     * @param int $offset
     */
    public function makeScreenshot($filename, $output, $offset)
    {
        shell_exec(sprintf('ffmpeg -ss %s -i %s -vframes 1 -f image2 %s', 
            $this->convertOffset($offset), $filename, $output));
    }
}
