<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/still", name="still")
     */
    public function takeStill(Request $request)
    {
        $videoId = $request->get('video_id');
        $offset = $request->get('offset', 0);

        $filename = $this->get('mi24.video_service')->generateScreenshot($videoId, $offset);

        return new JsonResponse([
            'videoId' => $videoId,
            'offset' => $offset,
            'url' => sprintf('%s/%s', $this->getParameter('screenshot_base_url'), $filename),
        ]);
    }
}
